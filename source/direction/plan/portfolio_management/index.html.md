---
layout: markdown_page
title: "Category Direction - Portfolio Management"
description: GitLab supports enterprise Agile portfolio and project management frameworks, including Scaled Agile Framework (SAFe), Scrum, and Kanban. Learn more!
canonical_path: "/direction/plan/portfolio_management/"
---

- TOC
{:toc}

## Portfolio Management

|                       |                               |
| -                     | -                             |
| Stage                 | [Plan](/direction/plan/)      |
| Maturity              | [Viable](/direction/maturity/) |
| Content Last Reviewed | `2023-01-16`


## Overview

### Purpose

GitLab's vision is to provide Portfolio Management tools for DevOps that help our customers manage a portfolio of work and determine which opportunities have higher ROI when making strategic business planning decisions. 

Enterprises work on complex initiatives that cut across multiple teams and departments, often spanning months, quarters, and even years. We support organizing initiatives into  multi-level work breakdown plans. We enable organizations to track efforts in flight and plan upcoming work to best utilize their resources and focus on the right priorities. 

GitLab supports popular [enterprise Agile portfolio and project management frameworks](https://about.gitlab.com/solutions/agile-delivery/), including Scaled Agile Framework [(SAFe)](https://about.gitlab.com/solutions/agile-delivery/scaled-agile/), [Scrum, and Kanban](https://about.gitlab.com/solutions/agile-delivery/).


|          |          |
| ---      | ---      |
| ![epicstree-direction.png](/direction/plan/portfolio_management/epicstree-direction.png)  | ![roadmaps-direction.png](/direction/plan/portfolio_management/roadmaps-direction.png)   |


### What's next & why

1. [Allow epics to have child epics and issues from different groups](https://gitlab.com/groups/gitlab-org/-/epics/8294) -  When working on large scale initiatives, contributions are needed from multiple teams which may be divided in different group hierarchies in GitLab. Today, issues and epics must be within the same group hierarchy to be added as children of an epic. We run into this limitation at GitLab when tracking work across our gitlab-com and gitlab-org groups. We will enable users to add epics from other hierarchies to facilitate tracking work across GitLab groups. 
1. [Converting epics to work items](https://gitlab.com/groups/gitlab-org/-/epics/6033) - Epics and Issues have different data elements and behaviors, which leads to confusion for our users. Epics are only available at the group level, which significantly decreases their reach. We will collaborate with the Project Management group to build the capabilities that Epics need into the Work Items framework. Our first iteration of this was building the concept of parent/child relationships which was used for [tasks](https://docs.gitlab.com/ee/user/tasks.html#tasks). 
1. [Maturing Health Status](https://gitlab.com/groups/gitlab-org/-/epics/2952) - An initial version of the Health Status was released and has received customer interest. Health Status offers a way for leaders to gain insight into risks or blockers that teams have run into. There are key enhancements that need to be implemented to make it a complete workflow and a great experience. 
1. [OKR Tracking](https://gitlab.com/groups/gitlab-org/-/epics/7864) - Today organizations (including GitLab) use a myriad of tools to track OKRs across the organization. This can span spreadsheets, presentations, text files in repositories, and in some cases purpose-built tools. They often also have the challenge of tying the Objectives and Key Results into initiatives (sometimes features, or software changes) - this requires careful integration between tool chains. The Product Planning group in tandem with a dedicated [SEG](https://about.gitlab.com/handbook/engineering/incubation/okr/) are working towards shipping an MVC for internal use as a validation step towards a viable feature we can generally release in 2023.

Future items include:

1. [Saved Queries and Views](https://gitlab.com/groups/gitlab-org/-/epics/5516) - It’s hard to query, save and share planning data with others. We plan to allow users to save a filtered list of their work item data so that they can easily come back to and share a list of work items. An example use cases is how at GitLab we frequently monitor our backlog for security issues and take that into account with every milestone plan. In the future, product managers and engineering team leads could have a shared saved query that surfaces all the security issues that have not been prioritized for their group.
1. Improvements to increase the maturity of [Roadmaps](https://gitlab.com/groups/gitlab-org/-/epics/2649) - Roadmaps are an industry standard way in which plans are visualized, and are key to an experience that is lovable by product and project managers. Key functionality like drag and drop editing are missing from our current implementation. 

## What we recently completed

1. [Allow sorting by health status in the issue list](https://gitlab.com/gitlab-org/gitlab/-/issues/377841) Now you can sort your filtered issues list by [health status](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#health-status) to quickly identify which issues need your attention.
1. [Allow user to update health status from issue board sidebar](https://gitlab.com/gitlab-org/gitlab/-/issues/371685) While you're grooming your issues board, you can now quickly update health status from the issue board's sidebar.
1. [Add existing item as child to work item](https://gitlab.com/groups/gitlab-org/-/epics/9184) As we burn down [Work Items - Add, remove and edit children records](https://gitlab.com/groups/gitlab-org/-/epics/9206), we've shipped the ability to search for an existing work item by title.
1. [Health Status `not` filter option on issue list](https://gitlab.com/gitlab-org/gitlab/-/issues/378460) When attempting to narrow down the scope of issues you need to review quickly, having flexible filter options is a game changer. In this release, we now have the ability to filter issues and work items using a `not` [issue list filter](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#filter-the-list-of-issues) for Health Status. This empowers users to quickly find records which are not `on track` as an efficient workflow.
1. [Epics can have child issues from different group hierarchies](https://gitlab.com/groups/gitlab-org/-/epics/8502) - As a continuation of improving cross-functional workflows in gitlab-org&8294, you can now add child epics from unrelated groups to a parent epic.
1. [Group minimum role should be Guest for epic relations](https://gitlab.com/groups/gitlab-org/-/epics/9232) - Since guest users can create issues and view epics, they should have the ability to relate epics to issues to ensure a complete workflow. In this milestone, we will relax permission requirements for adding parent records.
### What we're not doing

In the next two years:

- We plan to build a flexible planning tool that can be configured to implement SAFe. We do not plan to implement a system that is optimized for SAFe only. 
- We don't plan to build . We will continue to expose APIs for all our resources and they can be used for this use case by our customers.

## Maturity plan

Now that we've combined Roadmaps and Epics into this category, we need to **reset our category maturity** for Portfolio Management.

Previous maturities for reference: 

-  **Epics** are now a ~"type::feature" but was at the **viable** level, and our next maturity target was **complete** by 2021-10-30. Progress: [Viable](https://gitlab.com/groups/gitlab-org/-/epics/967) and [loveable](https://gitlab.com/groups/gitlab-org/-/epics/968)
-  **Roadmaps** are now a ~"type::feature" but was at the **minimal** level, and our next maturity target was **viable** by 2021-09-28 see [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/80922). We are tracking progress against this target via [this epic](https://gitlab.com/groups/gitlab-org/-/epics/1998). Progress: [Complete](https://gitlab.com/groups/gitlab-org/-/epics/2002)


## Competitive Analysis

Why Use GitLab for Portfolio Management?

Large enterprises have adopted Agile at enterprise scale through a variety of frameworks, including Scaled Agile Framework (SAFe), Spotify, and Large Scale Scrum (LeSS). GitLab enables teams to apply Agile practices and principles to organize and manage their work, whatever their chosen methodology.

As a single application for the complete DevOps lifecycle, GitLab is:

- **Seamless**: GitLab supports collaboration and visibility for Agile teams — from planning to deployment and beyond — with a single user experience and a common set of tools
- **Integrated**: Manage projects in the same system where you perform your work
- **Scalable**: Organize multiple Agile teams to achieve enterprise Agile scalability
- **Flexible**: Customize out-of-the-box functionality to the needs of your methodology, whether you're rolling your own flavor of Agile or adopting a formal framework
- **Easy to learn**: Check out [multi-team planning](https://www.youtube.com/watch?v=KmASFwSap7c) for Enterprise users
<br>

While GitLab users love it for its integrated DevSecOps workflows and low complexity user experience, we can improve our Enterprise Agile Planning tools to bring more value to the Program and Portfolio level user base. Please follow our roadmap of improvements in this area in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/7108).
### Portfolio Management Competitive Overview

##### Portfolio Management Category Best In Class Competitor

[**Jira Align**](https://www.atlassian.com/software/jira/align). Based on strong reviews from Gartner, Forrester, Peer Insights, and my own user review, Jira Align is the top competitor in the Enterprise Agile Planning space due to its full support for enterprise agile frameworks such as SAFe, excellent roadmapping, forecasting, dependency management, and collaboration capabilities. While Planview and Digital.ai also scored high with analysts, I found their UX heavy and dated. Jira Align scores well in portfolio management functionality and usability.

##### Top 3 Competitors

1. [Planview AgilePlace](https://www.planview.com/products-solutions/products/agileplace/). Planview's solutions enable organizations to connect the business from ideas to impact, empowering companies to accelerate the achievement of what matters most. Planview’s full spectrum of Portfolio Management and Work Management solutions create an organizational focus on the strategic outcomes that matter and empower teams to deliver their best work, no matter how they work. The comprehensive Planview platform and enterprise success model enables customers to deliver innovative, competitive products, services, and customer experiences. Headquartered in Austin, Texas, with locations around the world, Planview has more than 1,000 employees supporting 4,000 customers and 2.4 million users worldwide. For more information, visit www.planview.com. ([reference](https://www.linkedin.com/company/planview/))
2. [Digital.ai](https://digital.ai/). Based out of Boston, Massachusetts, Digital.ai is an industry-leading technology company dedicated to helping Global 5000 enterprises achieve digital transformation goals. The company's AI-powered DevOps platform unifies, secures, and generates predictive insights across the software lifecycle. Digital.ai empowers organizations to scale software development teams, continuously deliver software with greater quality and security while uncovering new market opportunities and enhancing business value through smarter software investments. ([reference](https://www.linkedin.com/company/digitaldotai/about/#:~:text=Digital.ai%20is,smarter%20software%20investments.))
3. [Jira Align](https://www.atlassian.com/software/jira/align/solutions). Jira Align (formerly known as AgileCraft) was acquired by Atlassian in early 2019. It's a cloud-based product that connects securely to one or more instances of Jira (of any flavour: Cloud, Server, or Data Center) to give insight into the state of play for all of the teams in an enterprise-level organisation. ([reference](https://www.adaptavist.com/blog/introduction-to-jira-align#:~:text=Jira%20Align%20(formerly,enterprise%2Dlevel%20organisation.)))

##### Compared Features

| Feature |Description |
| ------ |------|
| Portfolio financial management  |Visibility and insight into funding capacity rather than projects. Instead of determining how much it will cost to achieve the next two milestones, managers determine how much capacity is required to deliver a consistent flow of value. | 
|Portfolio level planning |  Identifying which programs to invest in, and how much. Portfolios are largely trying to figure out what initiative to fund, based on when the previous one is scheduled to finish.    | 
| Program level planning  |  Breaking large deliverables into chunks that make sense for each team and coordinating the teams' work. Programs need to worry about dependencies and coordination.   | 
| Enterprise agile framework (including SAFe) |SAFe support includes the processes, roles, and artifacts that enable scaling across teams, and the ability to plan and track work and assess economic benefits using at a minimum Portfolio SAFe in SAFe v. 5.0. EAP tools may support multiple enterprise agile frameworks commonly used in the industry.|  
| Forecasting |[A forecast is a calculation about the future completion of an item or items that includes both a date range and a probability.](https://www.scrum.org/resources/blog/agile-forecasting-techniques-next-decade#:~:text=A%20forecast%20is%20a%20calculation%20about%20the%20future%20completion%20of%20an%20item%20or%20items%20that%20includes%20both%20a%20date%20range%20and%20a%20probability.) Forecasts take the progress to date of all of the programs, then make forward-looking predictions.  |
|Dependency management|Dependencies are the relationships between work that determine the order in which the work items (features, stories, tasks) must be completed by Agile teams. Dependency management is the process of actively analyzing, measuring, and working to minimize the disruption caused by intra-team and / or cross-team dependencies. |
| Roadmapping |  [Roadmaps are the glue that link strategy to tactics. They provide all stakeholders with a view of the current, near-term, and longer-term deliverables that realize some portion of the Portfolio Vision and Strategic Themes.](https://www.scaledagileframework.com/roadmap/#:~:text=Roadmaps%20are%20the%20glue%20that%20link%20strategy%20to%20tactics.%20They%20provide%20all%20stakeholders%20with%20a%20view%20of%20the%20current%2C%20near%2Dterm%2C%20and%20longer%2Dterm%20deliverables%20that%20realize%20some%20portion%20of%20the%20Portfolio%20Vision%20and%20Strategic%20Themes.)  _© Scaled Agile, Inc._  | 
| End-to-end visibility to the value stream  | This capability indicates the tool’s ability to show the progress of software throughout the value stream from ideation through to production realization of the customer and business value.|
| Collaboration  |  Collaboration tools have the highest value for distributed organizations. These tools can range from virtual boards and team rooms to threaded conversations or advanced, work-item-context chat tools.  |

|Area of focus|GitLab|[Digital.ai](https://gitlab.com/gitlab-org/plan-stage/product-planning/competitive-research/-/issues/2#gitlab-vs-digitalai-agility-user-rating)|[Planview](https://gitlab.com/gitlab-org/plan-stage/product-planning/competitive-research/-/issues/3#gitlab-vs-planview-user-rating)|[Jira Align](https://gitlab.com/gitlab-org/plan-stage/product-planning/competitive-research/-/issues/4#gitlab-vs-jira-user-rating)|
|---|:---:|:---:|:---:|:---:|
| Portfolio financial management|⬜️ | 🟨|🟩 |🟨 |
| Portfolio level planning|⬜️ |🟩 |🟨 |🟨 |
| Program level planning| ⬜️|🟩 | 🟨| 🟩|
| Enterprise agile framework (including SAFe)| 🟨 |🟩 |🟩 |🟩 |
| Forecasting|⬜️ |🟨 |🟨 |🟩 |
| Dependency management| 🟨 |🟨 |🟨 |🟩 |
| Roadmapping| 🟨 |🟩 |🟨 |🟩 |
| End to end visibility to the value stream|🟨 |🟩 |🟨 |🟩 |
| Collaboration|🟩 |🟨 |🟨 |🟩 |

- ⬜️  lacking 
- 🟨  needs improvement
- 🟩  excels



