---
layout: handbook-page-toc
title: "Zip End Users Guide"
description: "Zip End Users Guide"
---

{::options parse_block_html="true" /}

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## What is Zip

Zip is an add-on to Coupa that will streamline the approval process to ensure we are gathering all the right information to manage the associated approvals, and get you the services you need as soon as possible. As a result, you will now have visibility into your purchase requests including the ability to see status and current stage of the approval process.

### How to access Zip

Zip is available via Okta. To access the platform:
1. Login to your [Okta home page](https://gitlab.okta.com/app/UserHome)
2. Find the `Zip` tile.
- _Zip should open in a new tab with your user logged in._
- _The Zip  main page should look like the one below._

![zip-image-1](/handbook/business-technology/enterprise-applications/guides/zip-guide/login-page.png)

**Note:** Check this [2 Minute Zip Overview Video](https://ziphq.wistia.com/medias/d7isqa87qz) to learn more about Zip and how to submit a purchase request.
{: .alert .alert-info}



## Zip - Getting Started


### How to initiate a new request?

1. Login to your [Okta home page.](https://gitlab.okta.com/app/UserHome)
2. Click on `+New Request` on the right side of the top menu.
3. Select the specific type of request needed.
4. Complete the requisition form

![zip-image-2](/handbook/business-technology/enterprise-applications/guides/zip-guide/new-request4.png)


![zip-image-3](/handbook/business-technology/enterprise-applications/guides/zip-guide/new-request6.png)


![zip-image-4](/handbook/business-technology/enterprise-applications/guides/zip-guide/new-request5.png)

**Note:** Depending on the selection for `What are you looking to purchase?`, the following question `Which detailed category best describes your purchase?` will update with the relevant response options.
{: .alert .alert-info}


### How to keep track of a request?

1. Login to your [Okta home page.](https://gitlab.okta.com/app/UserHome)
2. On the Zip home page click on `Requests` on the left side of the page.
- _A list of all the requests submitted appears._
- _There are multiple options to search a submitted request(Purchase name,vendor name,request #,etc)._
- _Drafted request are also accessible in this page._
3. Once the specific request has been selected, the approval workflow appears at the top of the page.

![zip-image-6](/handbook/business-technology/enterprise-applications/guides/zip-guide/submitted-request2.png)

**Note:** See which approvals are complete and which have yet to be completed. Automatic notifications via Slack and email will go out as the request is approved by different parties.
{: .alert .alert-info}


### How to comment on a request?

Every request detail page includes a `Comments` tab that provides a space for communication between all users who can access the request. In addition to comments from members of the organization, this tab provides notifications when comments are left by vendors on the vendor portal. 
An user can post a question or comment, and all of the stakeholders in the chain (the requester and all approvers) will be notified to view and then respond, or `@` tag a specific person and only that person will get notified.

![zip-image-7](/handbook/business-technology/enterprise-applications/guides/zip-guide/comments1.png)


## Zip - Approval Process

Some users identified by Legal, HR, Procurement, Security and Privacy have the option to approve or reject specific purchase request that are assigned to them.

### How to Approve a request?

1. You will receive an email / Slack alert when a new purchase request requires your approval. Notifications will highlight key information regarding the request, you can approve or reject the request from the notification directly.
2. From the notification, click on the View Request link to learn more and to view the request in Zip
3. Review the relevant information needed for the approval.
4. Click on your approval node in the review chain to view due dates, completed dates, any sub-tasks and integration information built-in.
5. Click on the section tabs to jump to the section of the page needed.

![zip-image-8](/handbook/business-technology/enterprise-applications/guides/zip-guide/approval-request7.png)

**Note:** You can set your approval notification preferences (Email and/or Slack) by logging into Zip, clicking: 
Settings -> Personal Settings -> Notifications.
{: .alert .alert-info}


![zip-image-9](/handbook/business-technology/enterprise-applications/guides/zip-guide/approval-request9.png)


![zip-image-10](/handbook/business-technology/enterprise-applications/guides/zip-guide/approval-request8.png)



### How to see all the requests that need my approval?

If you are the default assignee for your queue, you can view all pending requests for your approval by simply clicking `Dashboard` in the top right, on the Home dashboard page and selecting the `Needs My Approval` tab.

If you are not the default assignee for your queue, you can view all requests awaiting your queue’s approval by clicking `Dashboard` in the top right corner, selecting the `Queues` tab and selecting your queue. All requests in your queues will appear, you can update the `Status` filter if necessary to see previously approved or upcoming requests.

![zip-image-11](/handbook/business-technology/enterprise-applications/guides/zip-guide/approval-request4.png)


### How to reassign approvals?

You can reassign a pending approval from your queue to yourself or another user. Next to the request, click on the pencil icon to reassign the approval.

![zip-image-12](/handbook/business-technology/enterprise-applications/guides/zip-guide/approval-request5.png)


### How to see all of the documents associated with a request?

Click on the `Documents` tab at the top of any request, to see all the documents. You can view and manage all versions of documents associated with the purchase request.

![zip-image-13](/handbook/business-technology/enterprise-applications/guides/zip-guide/approval-request6.png)


## Zip Platform Support
- The **Procurement Team** should be the first point of contact for all **functionality** related questions. Examples are:
   - Why are we using Zip?
   - How do I see my requests in Zip / navigate Zip?
   - Which subsidiary or commodity do I select?
   - Do I have to use both Zip and Coupa to manage my purchase from request to payment?
   - How do I submit a virtual card request?
   - Status of requests
   - Where do I submit a PO Change Request?
   - Do I need to submit a new supplier request?
- The **Finance System Admins** should be the first point of contact for any **technical** issues and / or questions (eg. Access Requests, problems with login, bugs, etc).

**Note:** To contact the Procurement Team or Finance System Admins, send a message to [#zip-faq](https://gitlab.slack.com/archives/C04K1EJGLT1) with your question.
{: .alert .alert-info}



{::options parse_block_html="false" /}
